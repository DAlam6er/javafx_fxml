/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapp;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author student
 */
public class FXMLexampleController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    public void btnFindClick(ActionEvent event)
    {
        System.out.println("Find!");
    }
    
    @FXML
    public void btnCancelClick(ActionEvent event)
    {
        System.out.println("Try to Cancel");
    }
    
}
